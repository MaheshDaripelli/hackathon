import './App.css';
import {BrowserRouter as Router, Switch,Route, Redirect} from 'react-router-dom'
import NotFound from './components/NotFound';
import ForecastCard from './components/ForecastCard/forecastcard';
import { Home } from './components/Home';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/not-found" component={NotFound}/>
        <Redirect to="/not-found"/>
      </Switch>
    </Router>
  );
}

export default App;

import { Component } from "react";
import { ForecastCard } from "../ForecastCard";
import { TodayWeather } from "../TodayWeather";
import { WeekTable } from "../WeekTable";
import GridViewIcon from "@mui/icons-material/GridView";
import SsidChartIcon from "@mui/icons-material/SsidChart";
import { Divider } from "@mui/material";
import "./home.css";
// import axios from "axios";
import {ThreeCircles } from 'react-loader-spinner'
import NotFound from "../NotFound";
import { LineChart } from "../LineChart";
import { height } from "@mui/system";

const apiStatusConstants = {
  initial: "INITIAL",
  success: "SUCCESS",
  searchFailure: "SEARCHFAILURE",
  apiFailure: "APIFAILURE",
  inProgress: "IN-PROGRESS",
};
const days = {
  Today: "2022-08-15",
  Monday: "2022-08-16",
  Tuesday: "2022-08-17",
  Wednesday: "2022-08-18",
  Thursday: "2022-08-19",
  Friday: "2022-08-20",
  Saturday: "2022-08-21",
};
export class Home extends Component {
  state = {
    tempValue: "f",
    imageValue: true,
    forecast: [],
    apiStatus: apiStatusConstants.initial,
    location: [],
    inputSearch: "Hyderabad",
    tableData: [],
  };
  componentDidMount() {
    this.apiFetching();
  }
  apiFetching = async () => {
    this.setState({apiStatus:apiStatusConstants.inProgress})
    const { inputSearch } = this.state;
    const url = `https://api.weatherapi.com/v1/forecast.json?key=0b81c00069be4d3fa7262132220408&q=${inputSearch}&days=7&aqi=no&alerts=no`;
    const options = {
      method: "GET",
    };
    const response = await fetch(url, options);
    if (response.status == process.env.REACT_APP_SUCCESS_STATUS_CODE) {
      const data = await response.json();
      this.setState({
        forecast: data.forecast.forecastday,
        tableData: data.forecast.forecastday[0],
        apiStatus: apiStatusConstants.success,
        location: data.location,
      });
    } else if (
      response.status == process.env.REACT_APP_SEARCH_FAILURE_STATUS_CODE
    ) {
      this.setState({ apiStatus: apiStatusConstants.searchFailure });
    } else {
      this.setState({
        apiStatus: apiStatusConstants.apiFailure,
      });
    }
  };
  changeSearch = (event) => {
    if (event.key === "Enter") {
      this.setState({ inputSearch: event.target.value }, this.apiFetching);
    }
  };
  setTableData = (value) => {
    const { forecast } = this.state;
    for (let item of forecast) {
      if (item.date === days[value]) {
        this.setState({ tableData: item });
      }
    }
  };
  clickingRetry = () => {
    this.apiFetching();
  };
  onClickhandleUnit = (value) => {
    this.setState({ tempValue: value });
  };
  filterImage = (value) => {
    this.setState({imageValue:value})
  };
  homeView = () => {
    const { tempValue, imageValue, forecast, location, tableData } = this.state;
    return (
      <>
        <div className="forecast-div">
          <h1>
            {location.name} Weather Forecast{" "}
            <span>
              {location.region}, {location.country}
            </span>
          </h1>
          <div className="temperature-units">
            <p
              className={tempValue === "f" ? `unit active` : "unit"}
              onClick={() => this.onClickhandleUnit("f")}
            >
              <sup>o</sup>F
            </p>
            <p
              className={tempValue === "c" ? `unit active` : "unit"}
              onClick={() => this.onClickhandleUnit("c")}
            >
              <sup>o</sup>C
            </p>
          </div>
        </div>
        <div className="forecast-card-container">
          <ForecastCard forcastData={forecast} tempValue={tempValue} />
        </div>
        <div className="today-container">
          <TodayWeather
            name={location.name}
            forecast={forecast}
            tempValue={tempValue}
          />
        </div>
        <div className="day-main-container">
          <div className="day-container">
            {Object.keys(days).map((each) => {
              return (
                <>
                  <p
                    className="day-para"
                    style={{
                      height: "100%",
                      width: "100%",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    onClick={() => this.setTableData(each)}
                  >
                    {each}
                  </p>
                  <Divider
                    orientation="vertical"
                    style={{
                      borderColor: "#95eaf5",
                    }}
                  />
                </>
              );
            })}
          </div>
          <div className="image-filter">
          <button style={{
              cursor:"pointer",
              borderWidth:"0px",
              backgroundColor:"transparent",
              height:"100%",
              width:"100%"
            }}
              onClick={()=>this.filterImage(true)}
              className={imageValue ? "filter-color" : ""}>
            <GridViewIcon/>
            </button>
            <Divider orientation="vertical"
                    style={{
                      borderColor: "#bee0ed",
                    }}/>
                  <button style={{
              cursor:"pointer",
              borderWidth:"0px",
              backgroundColor:"transparent",
              height:"100%",
              width:"100%"
            }}
              onClick={()=>this.filterImage(false)}
              className={imageValue ? "" : "filter-color"}>
            <SsidChartIcon
            
            />
            </button>
          </div>
        </div>
        <div className="weekly-table">
          {
            imageValue ? <WeekTable forecast={tableData} tempValue={tempValue} /> : <div className="line-chart"><LineChart forecast={tableData} tempValue={tempValue}/></div>
          }
          
        </div>
      </>
    );
  };
  apiFailView = () => {
    return (
      <div>
        <div className="videosDiv1">
          <img
            src="https://assets.ccbp.in/frontend/react-js/nxt-watch-failure-view-light-theme-img.png"
            alt="failure view"
            className="img10"
          />
          <h1>Oops! Something Went Wrong</h1>
          <p>unable to connect to the server</p>
          <button
            type="button"
            className="button6"
            onClick={this.clickingRetry}
          >
            Retry
          </button>
        </div>
      </div>
    );
  };
  noLocationFoundView = () => {
    return <NotFound />;
  };
    LoadingView = () => (
    <div className="products-loader-container">
      <ThreeCircles  color="#879799" height="150" width="150" />
    </div>
  )
  renderHomePage = () => {
    const { apiStatus } = this.state;

    switch (apiStatus) {
        case apiStatusConstants.inProgress:
        return this.LoadingView();
      case apiStatusConstants.success:
        return this.homeView();
      case apiStatusConstants.searchFailure:
        return this.noLocationFoundView();
      case apiStatusConstants.apiFailure:
        return this.apiFailView();
      default:
        return null;
    }
  };

  render() {
    return (
      <div className="home-main-container">
        <input
          type="search"
          className="input-search"
          placeholder="Please Enter City Name, US Zip Code,Canada Postel Code, UK PostCode, ip metar, etc."
          onKeyUp={this.changeSearch}
        />
        {this.renderHomePage()}
      </div>
    );
  }
}

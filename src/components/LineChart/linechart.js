import React from "react";

import { Chart } from "react-chartjs-2";
import { Chart as ChartJS, registerables } from 'chart.js';
ChartJS.register(...registerables);


export const LineChart = (props)=>{
  const{forecast,tempValue} = props
  const{hour} = forecast
  const weatherData = tempValue === "c" ? [hour[0].temp_c, hour[2].temp_c, hour[5].temp_c, hour[8].temp_c, hour[11].temp_c, hour[14].temp_c,hour[17].temp_c]:[hour[0].temp_f, hour[2].temp_f, hour[5].temp_f, hour[8].temp_f, hour[11].temp_f, hour[14].temp_f,hour[17].temp_f] 
  const data = {
    labels: ["0:00 am", "3:00 am", "6:00 am", "9:00 am", "12:00 pm", "3:00 pm", "6:00 pm"],
    datasets: [
      {
        responsive:true,
        label: "Today's Weather",
        data: weatherData,
        fill: true,
        backgroundColor: "Transparent",
        borderColor: "rgba(75,192,192,1)"
      },
    ]
  };
  const options = {
    scales: {
      y:
        {
          min: 10,
          max: 100,
          stepSize: 10,
          ticks:{
            callback:function(label){
              if(tempValue === "c"){
                return label + String.fromCodePoint(8451)
              }else{
                return label + "\u2109"
              }
            }
          }
        },
      }
  }
  return (
    <div className="App">
      <Chart type="line" data={data} options={options} />
    </div>
  );
}

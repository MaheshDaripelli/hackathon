import './forecastcard.css'

export const ForecastCard = (props) => {
    const {forcastData,tempValue} = props
    return (
        <div className='forecast-container'>
            <div className='head-container'>
                <div className='head-left-container'>
                    <img src={forcastData[0].day.condition.icon} className='weather-image' />
                    <div>
                        {
                            tempValue === "c" ? <h1 className='sunday-class'>
                            {forcastData[0].day.avgtemp_c} &deg;C Sunday
                        </h1> : <h1 className='sunday-class'>
                            {forcastData[0].day.avgtemp_f} &deg;F Sunday
                        </h1>
                        }
                        <p className='rain-para'>{forcastData[0].day.condition.text}</p>
                    </div>
                </div>
                <div className='head-right-container'>
                    <div>
                        <p>
                            Wind
                        </p>
                        <p>
                            {forcastData[0].day.maxwind_mph} mph
                        </p>
                    </div>
                    <div>
                        <p>
                            Precip
                        </p>
                        <p>
                            {forcastData[0].day.totalprecip_mm} mm
                        </p>
                    </div>
                    <div>
                        <p>
                            Pressure
                        </p>
                        <p>
                            11.2 mph
                        </p>
                    </div>
                </div>
            </div>
            <div className='forecast-body-container'>
                <div className='weekly-cloud'>
                    <img src={forcastData[1].day.condition.icon} className='weather-sub-image' />
                    <p className='weekly-para'>Monday</p>
                    {
                        tempValue === "c" ? <p className='weekly-para'>{forcastData[1].day.avgtemp_c}&deg; C</p> : <p className='weekly-para'>{forcastData[1].day.avgtemp_f}&deg; F</p>
                    }
                    <p className='weekly-para'>{forcastData[1].day.condition.text}</p>
                </div>
                <div className='weekly-cloud'>
                    <img src={forcastData[2].day.condition.icon} className='weather-sub-image' />
                    <p className='weekly-para'>Tuesday</p>
                    {
                        tempValue === "c" ? <p className='weekly-para'>{forcastData[2].day.avgtemp_c}&deg; C</p> : <p className='weekly-para'>{forcastData[2].day.avgtemp_f}&deg; F</p>
                    }
                    <p className='weekly-para'>{forcastData[2].day.condition.text}</p>
                </div>
                <div className='weekly-cloud'>
                    <img src={forcastData[3].day.condition.icon} className='weather-sub-image' />
                    <p className='weekly-para'>Wednesday</p>
                    {
                        tempValue === "c" ? <p className='weekly-para'>{forcastData[3].day.avgtemp_c}&deg; C</p> : <p className='weekly-para'>{forcastData[3].day.avgtemp_f}&deg; F</p>
                    }
                    <p className='weekly-para'>{forcastData[3].day.condition.text}</p>
                </div>
                <div className='weekly-cloud'>
                    <img src={forcastData[4].day.condition.icon} className='weather-sub-image' />
                    <p className='weekly-para'>Thursday</p>
                    {
                        tempValue === "c" ? <p className='weekly-para'>{forcastData[4].day.avgtemp_c}&deg; C</p> : <p className='weekly-para'>{forcastData[4].day.avgtemp_f}&deg; F</p>
                    }
                    <p className='weekly-para'>{forcastData[4].day.condition.text}</p>
                </div>
                <div className='weekly-cloud'>
                    <img src={forcastData[5].day.condition.icon} className='weather-sub-image' />
                    <p className='weekly-para'>Friday</p>
                    {
                        tempValue === "c" ? <p className='weekly-para'>{forcastData[5].day.avgtemp_c}&deg; C</p> : <p className='weekly-para'>{forcastData[5].day.avgtemp_f}&deg; F</p>
                    }
                    <p className='weekly-para'>{forcastData[5].day.condition.text}</p>
                </div>
                <div className='weekly-cloud'>
                    <img src={forcastData[6].day.condition.icon} className='weather-sub-image' />
                    <p className='weekly-para'>Saturday</p>
                    {
                        tempValue === "c" ? <p className='weekly-para'>{forcastData[6].day.avgtemp_c}&deg; C</p> : <p className='weekly-para'>{forcastData[6].day.avgtemp_f}&deg; F</p>
                    }
                    <p className='weekly-para'>{forcastData[6].day.condition.text}</p>
                </div>
            </div>
        </div>
    )
}
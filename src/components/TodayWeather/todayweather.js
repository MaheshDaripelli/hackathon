import './todayweather.css'

export const TodayWeather = (props) => {
    const{forecast,tempValue} = props
    return (
        <div>
            <h1>Today's Weather In {props.name}</h1>
            <div className='today-weather-container'>
                <div>
                    <p>Sunrise {forecast[0].astro.sunrise}</p>
                    <p>Sunset {forecast[0].astro.sunset}</p>
                </div>
                <div>
                    <p>Moonrise {forecast[0].astro.moonrise}</p>
                    <p>Moonset {forecast[0].astro.moonset}</p>
                </div>
                <div>
                    <p>Max.</p>
                    {
                        tempValue === "c" ? <p>{forecast[0].day.maxtemp_c} &deg;C</p> : <p>{forecast[0].day.maxtemp_f} &deg;F</p>
                    }
                </div>
                <div>
                    <p>Min.</p>
                    {
                        tempValue === "c" ? <p>{forecast[0].day.mintemp_c} &deg;C</p> : <p>{forecast[0].day.mintemp_f} &deg;F</p>
                    }
                </div>
                <div>
                    <p>Avg.</p>
                    {
                        tempValue === "c" ? <p>{forecast[0].day.avgtemp_c}</p> : <p>{forecast[0].day.avgtemp_f}</p>
                    }
                </div>
                <div>
                    <p>Precip.</p>
                    <p>{forecast[0].day.totalprecip_mm} mm</p>
                </div>
                <div>
                    <p>Max.Wind</p>
                    <p>{forecast[0].day.maxwind_kph} kph</p>
                </div>

            </div>
        </div>
    )
}

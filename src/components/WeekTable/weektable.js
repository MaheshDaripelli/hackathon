import { useEffect } from 'react'
import './weektable.css'
const timeLine = {
  morning: "am",
  afternoon: "pm"
}
const timings = {
 "0:00 am":{},
 "3:00 am":{},
 "6:00 am":{},
 "9:00 am":{},
 "12:00 pm":{},
 "3:00 pm":{},
 "6:00 pm":{},
 "8:00 pm":{},
}

export const WeekTable = (props) => {
  let item = 0
  const { forecast, tempValue } = props
  return (
    <div className="">
      <table className="table">
        <tr className="table-headings">
          <th>
            07
            <br />
            Sunday
          </th>
          <th>Icon</th>
          <th>Temp</th>
          <th>Wind</th>
          <th>Precip.</th>
          <th>Cloud</th>
          <th>Humidity</th>
          <th>Pressure</th>
        </tr>
        {
          Object.keys(timings).map(each => {
            item = item + 1
            return (
              <tr>
                <th>{each}</th>
                <td>
                  <img
                    src={forecast.hour[item].condition.icon}
                    alt="cloud"
                  />
                </td>
                <td>
                  <p>
                    {tempValue === "c"
                      ? forecast.hour[item].temp_c 
                      : forecast.hour[item].temp_f }
                    {tempValue === "c" ? (
                      <>
                        <sup>o</sup>C
                      </>
                    ) : (
                      <>
                        <sup>o</sup>F
                      </>
                    )}
                  </p>
                </td>
                <td>
                  <p>
                    {forecast.hour[item].wind_kph}{" "}
                    Kmph
                  </p>
                </td>
                <td>
                  {forecast.hour[item].precip_mm} mm
                </td>
                <td>
                  {forecast.hour[item].cloud}%
                </td>
                <td>
                  {forecast.hour[item].humidity}%
                </td>
                <td>
                  {forecast.hour[item].pressure_in}{" "}
                  in
                </td>
              </tr>
            )
            
          })
        }

      </table>
    </div>
  )
}
